// globale Variablen
var number_of_players = 0; 																																	 // in unserem Fall zwischen 1 und 4
var i; 																																											 // Z�hlvariable für alle �u�eren Schleifen
var j;																																											 // Z�hlvariable für alle inneren Schleifen
var distance = 10;
var player = new Array(); 																																	 //speichert , wer spielt, und gibt für jedem Spieler eine Zahl, hilft einfach für Hintergrund des Spiels zu zeichnen.
var player_width = 150;																																			 //gr��e von Pigeon
var player_height = 191;
var count=0;																																								 // z�hler die Anzahl der Spielern
// Alle Bitmaps
var	player1 = new createjs.Bitmap('img/spieler/doctor.png');
var	player2 = new createjs.Bitmap('img/spieler/pirate.png');
var	player3 = new createjs.Bitmap('img/spieler/sailer.png');
var	player4 = new createjs.Bitmap('img/spieler/maedchen.png');
var background_picture = new createjs.Bitmap('img/bilder/background.png')
var tipp_1 = new createjs.Bitmap('img/bilder/hinweise1.png');
var tipp_2 = new createjs.Bitmap('img/bilder/hinweise2.png');
var	ufo_begin = new createjs.Bitmap('img/spieler/ufo.png');


function choose_players()
{
	background_picture.x=0;																																			//hintergrund des Men�s
	background_picture.y=0;
	stage.addChild(background_picture);

	tipp_1.x=2*distance;
	tipp_1.y=2*distance;
	tipp_1.visible=true;
	stage.addChild(tipp_1);
	tipp_2.x=2*distance;
	tipp_2.y=2*distance;
	tipp_2.visible=false;
	stage.addChild(tipp_2);

	ufo_begin.x = screen.width/2-360;
	ufo_begin.y = screen.height/2-150;
	ufo_begin.setBounds(ufo_begin.x,ufo_begin.y,720,100);
	stage.addChild(ufo_begin);


	player1.x = screen.width/2 - 1.5*distance - player_width - 3*distance - player_width;				//spieler erzeugen
	var begin_position_x =	screen.width/2 - 1.5*distance - player_width - 3*distance - player_width;
	player1.y = screen.height - player_height ;
	var begin_position_y =	screen.height - player_height ;
	player1.setBounds(player1.x, player1.y, player_width, player_height);

	stage.addChild(player1);
	var choose1=false; 																																				//am Anfang wird der Spieler 1 noch nicht gew�hlt

	player2.x = player1.x + player_width + 30;
	player2.y = player1.y ;
	player2.setBounds(player2.x, player2.y, player_width, player_height);
	stage.addChild(player2);
	var choose2=false;

	player3.x = player2.x + player_width + 30 ;
	player3.y = player1.y ;
	player3.setBounds(player3.x, player3.y, player_width, player_height);
	stage.addChild(player3);
	var choose3=false;

	player4.x = player3.x + player_width + 30 ;
	player4.y = player3.y;
	player4.setBounds(player4.x, player4.y, player_width, player_height);
	stage.addChild(player4);
	var choose4=false;

	for(i=0;i<4;i++)
	{
		player[i]=0;																																							//speichert im Array für jeder Spieler eine Zahl
																																															//es hilft dann später für itembox_verteilung sowie hintergrund des Spielfeldes
	}
	stage.update();

																																															//player ziehen
	player1.on("pressmove", move);
	player2.on("pressmove", move);
	player3.on("pressmove", move);
	player4.on("pressmove", move);


	player1.on("pressup", function (evt) {
		reset_idle();																																						  //player loslassen mit Hilfe von pressup_event
		if(intersect(evt.currentTarget, ufo_begin))																								// wenn der Spielerfigur auf das Box gezogen und losgelassen wird, dann steht er auf ufo_begin
		{
			player1.x = ufo_begin.x + 4*distance ;
			player1.y = ufo_begin.y ;

			if (choose1==false){																																		// Spielerfigur 1 ist noch nicht ausgew�hlt ( noch nicht auf ufo_begin stehen)
				choose1=true;
				count=count+1;
				player[0]=1;																																					// Spielerfigur 1 hat die Nummer 1
				Start.visible=true;
				tipp_1.visible=false;
				tipp_2.visible=true;

			}
		}
		else 																																											// der Spielerfigur wird irgendwo au�er ufo_begin gezogen und losgelassen
		{
			player1.x = screen.width/2 - 1.5*distance - player_width - 3*distance - player_width;		// bleibt auf Anfangsposition
			player1.y = screen.height - player_height ;
			if(choose1==true)																																				// der Spielerfigur 1 ist schon gew�hlt (schon auf ufo_begin) und jetzt auf anderen Platz hingezogen
				{
					choose1=false;
					count=count-1;
					player[0]=0;
				}
			if(count==0)
				{

					Start.visible=false;
					tipp_1.visible=true;
					tipp_2.visible=false;

				}
		}
		stage.update(evt);


	});
	player2.on("pressup", function (evt) {
		reset_idle();
		if(intersect(evt.currentTarget, ufo_begin)){
			player2.x = ufo_begin.x + 4*distance + player_width + 2*distance;
			player2.y =ufo_begin.y ;
			if (choose2==false){
				choose2=true;
				count=count+1;
				player[1]=2;																																						//Player 2 hat Nummer 2
				Start.visible=true;
				tipp_1.visible=false;
				tipp_2.visible=true;

			}
		}
		else
		{
				player2.x = begin_position_x +30 + player_width + 30 ;
				player2.y = begin_position_y;
			if(choose2==true)
				{
					choose2=false;
					count=count-1;
					player[1]=0;
				}
			if(count==0)
				{
					Start.visible=false;
					tipp_1.visible=true;
					tipp_2.visible=false;

				}
		}
		stage.update(evt);
	});
	player3.on("pressup", function (evt) {
		reset_idle();
		if(intersect(evt.currentTarget, ufo_begin)){
			player3.x = ufo_begin.x + 4*distance + 2* player_width+4*distance;
			player3.y = ufo_begin.y;


			if (choose3==false){
				choose3=true;
				count=count+1;
				player[2]=3;																																					//Player 3 hat Nummer 3
				Start.visible=true;
				tipp_1.visible=false;
				tipp_2.visible=true;

			}
		}
		else
		{
				player3.x = begin_position_x + player_width + 30 + player_width + 30 ;
				player3.y = begin_position_y;
			if(choose3==true)
				{
					choose3=false;
					count=count-1;
					player[2]=0;
				}
			if(count==0)
				{
					Start.visible=false;
					tipp_1.visible=true;
					tipp_2.visible=false;
				}
		}
		stage.update(evt);
	});
	player4.on("pressup", function (evt) {
		reset_idle();
		if(intersect(evt.currentTarget, ufo_begin)){
			player4.x = ufo_begin.x + 4*distance + 3*player_width+6*distance;
			player4.y = ufo_begin.y;
			if (choose4==false){
				choose4=true;
				count=count+1;
				player[3]=5;																																					//Player 4 hat Nummer 5, damit es 2+3=4+1 vermeidet
				Start.visible=true;
				tipp_1.visible=false;
				tipp_2.visible=true;
			}
		}
		else
		{
			player4.x = begin_position_x + player_width + 30 + player_width + 30 + player_width + 30 ;
			player4.y = begin_position_y;
			if(choose4==true)
				{
					choose4=false;
					count=count-1;
					player[3]=0;
				}
			if(count==0)
				{
					Start.visible=false;
					tipp_1.visible=true;
					tipp_2.visible=false;

				}
		}
		stage.update(evt);
	});

	// Knopf--------------------------------------

		var Start = new createjs.Bitmap('img/bilder/start.png');
		Start.x = screen.width-180-((screen.width-720-180)/2)/2
		Start.y = screen.height*0.45;
		Start.setBounds(Start.x,Start.y,180,174);
		Start.visible=false;
		stage.addChild(Start);
		Start.addEventListener("click", click_start);

	stage.update();

	show_pictures();
}

function oneorteam(stage)																																			//Men� für Zusammenspielen oder Gegenspielen
{ 	//TODO es ist hier Platz für Erweiterung des Spiels

	stage.update();

	szenario(stage);
}


function click_start(event) 																																	//Clickevent von Startbutton
{
		if(count>0) {																																							//man kann nur klicken, wenn Anzahl der Spielern > 0 ist
			number_of_players=count;
			player1.removeAllEventListeners();
			player2.removeAllEventListeners();
			player3.removeAllEventListeners();
			player4.removeAllEventListeners();
			stage.removeAllEventListeners();
			stage.removeAllChildren();
			oneorteam(stage);

		}
		reset_idle();																																							//Timer wird wieder gesetzt.
		stage.update();
}
