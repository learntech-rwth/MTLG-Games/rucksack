//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// STERNE ~ Variablen # Position, Farbe...
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var star_coords_a = new Array(); 																			//koords der sterne im rucksack	
var stars_a = new Array();
var star_quantity = 0; 																								//aktuelle anzahl an sternen 
var star_quantity_new = star_quantity;																//zukueftige anzahl an sternen
var star_quantity_max = 19; 																					//12 aussen + 6 innen + 1 mitte=19; 
var turn_speed = 3; 																									//drehgeschwindigkeit der im rucksack
var star_rel_pos_out = 0.7;																						//radius-faktor zum positionieren aussen (12)
var star_rel_pos_in = star_rel_pos_out/2;															//radius-faktor zum positionieren innen (6)
var stars_big_circle_max=12;																					//darstellbare sterne im rucksack aussen
var stars_small_circle_max =parseInt(stars_big_circle_max/2+0.5);//darstellbare sterne im rucksack innen
var turning_stars_a = new Array();					
var alpha_divisor = 7;																								//faktor fuer alphawert (unsichtbarkeit) der sterne im rucksack

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// STERNE ~ Variablen # Rotation, Zeichnung...
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var drawGraphics;
var points;
var angle = Math.PI/36;
var matrix = new createjs.Matrix2D();
var _point = new createjs.Point();
var points2D = new Array();
var focal_length = 300; 																							//brennweite
var speed_max = 19;
const color_switch_time = 50;
var color_switch_time_counter = color_switch_time;
var last_rgb_r;
var last_rgb_g;
var last_rgb_b;
var rgb_r;
var rgb_g;
var rgb_b;

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// STERNE ~ Funktionen
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function set_sterne_coords()  																				// sternen koordinaten setzen
{	var tmp_angle_half	=	0; 																						//halben winkel
	var tmp_angle_full	=	0;																						//ganzen winkel
	var sterne_winkel = 360/stars_big_circle_max;												//winkelabstand von den 12 aeusseren sternen
	var sterne_winkel_innen = 360/stars_small_circle_max; 							//winkelabstand von den 6 inneren sternen
	for (i = 0; i < star_quantity_max; i++)  														//alle sterne durchgehen
	{	var ergebnis = new coords(0,0); 																	//koordinaten-variable
		tmp_angle_full += sterne_winkel; 																	//im uhrzeigersinn ganzen winkel weiter
		tmp_angle_half += 0.5*sterne_winkel; 															//halben winkel weiter
																																			//a=radius*sin(alpha)+x
		ergebnis.x =  sinus(tmp_angle_half)*r_in*star_rel_pos_out + center_c.x; //
																																			// b=y-cos(alpha)*radius 
		ergebnis.y = center_c.y - cosinus(tmp_angle_half)*r_in*star_rel_pos_out; 
		star_coords_a[i]=ergebnis;
		tmp_angle_half = tmp_angle_full;
		if(i==stars_big_circle_max-1) 																		//wenn die ersten 12 sterne (aussen) fertig sind, winkel-einheit und radius anpassen
		{	tmp_angle_full=0; 																							// halbe winkel
			tmp_angle_half=0; 																							// ganze winkel
			sterne_winkel = sterne_winkel_innen; 														//winkel wird groesser weil 12tel zu 6tel 
			star_rel_pos_out = star_rel_pos_in; 														//radius per multiplikator verkleinern 
		}
		else if (i==star_quantity_max-1) star_coords_a[i]=center_c; 			//letzter stern ist im zentrum des rucksacks
	}
	star_rel_pos_out = 2*star_rel_pos_in; 															//wieder auf standart-wert zuruecksetzen (globale variable)			
}

function star_init() 																									//initialisierungsfunktioon der drehenden sterne im rucksack
{	set_sterne_coords(); 																								//koordinaten der 19 sterne setzen
	star_force_update(); 																								//einmaliges forciertes befuellen des sterne-arrays		
																																			//5 spitzenanzahl, 25 (berg)spitzenlaenge , 10 (tal)spitzenlaenge
																																			//points ist ein array, das 3D-koordinaten von allen  bergspitzen und talspitzen enthaelt
	points = createStarPoints(5, 25, 10);
	
																																			//diese 3D koordinaten werden bei draw() benutzt, um per segmentzeichnung diese alle zu verbinden, sodass
																																			//am ende ein stern dargestellt wird
	draw(points,stars_a); 
	
																																			//ticker zum drehen der sterne
	createjs.Ticker.addEventListener("tick", rotate_star_surface);
																																			//bei jedem tick werden die 3D-koordinaten im points-array geandert
	stage.addEventListener("tick", star_turning_speed);
}	

function Point3D(x, y, z) 																						//3D-point konstruktor
{	this.x = x;
	this.y = y;
	this.z = z;
}

Point3D.prototype.getProjetedPoint = function(focal_length) 					//jede 3D-Point bekommt eine Methode getProjetedPoint, nachtr�gliche Erweiterung des Kontruktors
{				var point2D = new createjs.Point();
				var w = ( focal_length / (focal_length + this.z) );
				point2D.x = this.x * w;																				//vorige im Kontruktor erzeugte Variable ausnutzen
				point2D.y = this.y * w;
				return point2D;																								//R�ckgabe als 2D-Point
};

function star_turning_speed(eventObject) 
{	angle = Math.min(turn_speed,speed_max);
}

function rotate_star_surface(eventObject)  														// wird regelmaessig (ticker) ausgefuerht: drehung via matrix
{	var count = points.length;
	points2D.length = 0; 																								//array resetten
																																			//Matrix2D.transformPoint() method transforms xy coordinates of Point objects. 
																																			//rotieren von x und z von 3D koordinaten mit besagter methode. 
																																			//dann um die y achse rotieren
	matrix.identity().rotate(angle); 
																																			//identity() setzt eigenschaften einer einheitsmatrix zu gegebener identitaetsmatrix (neutrales element bzgl. matrixmultiplikation)
	for (var i = 0; i < count; i++) 
	{	var point = points[i];
		matrix.transformPoint(point.x, point.z, _point); 									//parameter x, y , zielort zum einfuegen des ergebnisses
		point.x = _point.x;
		point.z = _point.y; 																							//obigen parameter korrigieren
		points2D[i] = point.getProjetedPoint(focal_length); 							//neuer matrixeintrag abhaengig von der brennweite
	}
	draw(points2D,stars_a);
}

function createGraphics(x, y,i) 																			//"konstruktor" fuer sterne, erzeugt shape auf koordinaten mit index 'i' TODO name 
{	turning_stars_a[i] = new createjs.Shape(); 													//shape
	turning_stars_a[i].x = x;																						// koordinaten
	turning_stars_a[i].y = y; 																					//
	stage.addChild(turning_stars_a[i]); 																//der stage hinzufuegen
	return	turning_stars_a[i].graphics;																//shape.graphics zurueckgeben
}
function draw(points, stars_a)
	{	
			for (i = 0; i < stars_a.length;i++) 														//alle stars_a durchgehen
			{ var count = points.length; 																		
				var point = points[count - 1]; 																//vom letzten Arrayeintrag weiter zaehlen
				var drawed_star_alpha =0.9;
				if (star_quantity == star_quantity_max) 											
				{	
					if(color_switch_time_counter<=0)
					{ last_rgb_r = Math.floor(Math.random()*255); 							//untere gauss-klammer von random zwischen 0 bis 1 mal 255
						last_rgb_g = Math.floor(Math.random()*255);
						last_rgb_b = Math.floor(Math.random()*255); 							//wenn alle sterne, zufallsfarben, signalisiert erfolgreiche kombination
						rgb_r	= last_rgb_r;
						rgb_g = last_rgb_g;
						rgb_b = last_rgb_b; 
						color_switch_time_counter = color_switch_time;
					}
					color_switch_time_counter--;
				}
				else
				{	rgb_r= 255;
					rgb_g = 140;
					rgb_b = 0; 																									//volle sterne sind orange mit alpha 0.9 (10% durchsichtig)
				}
				if (i>=star_quantity) 																				//ab jetzt leere sterne
				{	drawed_star_alpha /= alpha_divisor; 												//alphaewert von grauen sternen kleinerscaliert
 					rgb_r=255; 
					rgb_g=255; 
					rgb_b=255; 																									//leere sterne
				}	
			  stars_a[i].clear() 																						//vorherigen gezeichneten rotationswinkel entfernen
						.beginStroke(createjs.Graphics.getRGB(rgb_r,rgb_g,rgb_b,drawed_star_alpha))//farbe und alphawert uebernehmen
						.setStrokeStyle(3)																				//liniendicke
						.moveTo(point.x, point.y)																	//cursor startpunkt der zeichnung des sternes bewegen
				for (var j = 0; j < count; j++) 
				{	point = points[j];
					stars_a[i].lineTo(point.x, point.y);												//das eigentliche zeichnen der rotierenden sterne
				}
			}
				stage.update();																								
	}

function createStarPoints(peak_quantity, radius_hill, rarius_valley) 	//array mit 3D-koordinaten fuellen, von den spitzen der sterne (berg- und talspitzen)
{	var peak_coords_a = new Array(); 																		//das zu fuellende array
	var tmp_angle = Math.PI*(-0.5); 																		//-90�
	var theta = tmp_angle*2 / peak_quantity; 														//-180� * 5 Zacken
	for (var i = 0; i < peak_quantity; i++) 														//alle (berg)spitzen durchgehen
	{	peak_coords_a.push(new Point3D(radius_hill * Math.cos(tmp_angle), radius_hill * Math.sin(tmp_angle), 0)); //koordinaten der bergspitzen
		tmp_angle += theta; 																							//winkel im uhrzeigersinn inkrementieren
		peak_coords_a.push(new Point3D(rarius_valley * Math.cos(tmp_angle), rarius_valley * Math.sin(tmp_angle), 0)); //koordinaten der talspitzen
		tmp_angle += theta; 																							//winkel im uhrzeigersinn inkrementieren, hier ist winkel naechster bergspitze
	}
	return peak_coords_a; 																							//array zurueckgeben
}
	























