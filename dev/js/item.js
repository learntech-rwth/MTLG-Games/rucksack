//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ITEM ~ Variablen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var item_coords_distr_a = new Array();
var item_coords_quantity_v = 24;
var picture_name_a;
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ITEM ~ Funktionen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

function import_all_png(string_from_html)
{	picture_name_a = string_from_html.split(";");																											// aufteilen des lange strings
	var manifest_builder = new Array();																																// lokales array
	var i;
	for (i = 0; i < picture_name_a.length-1; i++) {																										// fuelle array mit src und id fuer jedes Bild aus Ordner
																																																		// -1, weil picture_name_a auch der leeren Wert nach letzten ";" speichert
		manifest_builder.push({src: picture_name_a[i], id: picture_name_a[i]}); 
	}
	return manifest_builder;																																					//  Array von Typ[{src:Bildname,id},...]
}

function handle_manifest(event)
{
	var image = event.result;																																					// uebergebe geladenes Image an lokale Variable
	var bitmap = new createjs.Bitmap(image);																													// Parse Image als bitmap
	bitmap.regX = bitmap.getBounds().width/2;
	bitmap.regY = bitmap.getBounds().height/2;
	_cache_array.push(bitmap);																																				// push Bitmap in _cache_array
}


function set_coords_item_distribution() 																														//koordinaten fuer item verteilung bestimmen
{  																																																 	///linke haelfte des arrays ist linke seite des spielfeldes							//
																																																		///	rechte seite des array ist rechte haelfte des spielfeldes					 //
																																																		///	jedes viertel des arrays ist jeweils der spielbereich eines spielers//
	var ref_point_c = new coords(0,0);																																//referenzpunkt koordinate oben links
	var horizontal_3 = 0; 																																						//spalten
	var axis_inverter=1;																																							// 1 wenn links, -1 wenn rechts
	var vertical_4=1; 																																								//zeilen
	for(i=0;i<item_coords_quantity_v;i++)
	{	horizontal_3++;																																									//spalten hochzaehlen
		if (horizontal_3>3)																																							//periodisch 1 bis 3
		{ horizontal_3 =1; 																																							//spalten reset
			vertical_4++;																																								  //zeile hochzaehlen
		}
		if (vertical_4>4) vertical_4 =1; 																																//periodisch 1 bis 4
		if(i==item_coords_quantity_v/2) 																																// wenn erste haelfte des arrays befuellt
		{	ref_point_c.x=screen.width; 																																	//referenzpunktwechsel nach oben rechts von oben links
			axis_inverter =-1; 																																						//achsenwechsel, weil anderer referenzpunkt
		}
		var tmp_point_c = new coords(ref_point_c.x+axis_inverter*(horizontal_3*screen.width/12),ref_point_c.y + vertical_4*screen.height/6);

		item_coords_distr_a[i]=tmp_point_c;																															//ergebnis pushen
	}
	listitemcoords24() 	;
}

function reposition_item(item_shape,item_png, dropnode)
{ if(dropnode === -1) NOOP();	 																																			//"ausweich-index" signalisiert nichts zu tun
	else
	{ item_shape.x=dropnode.x;   																																			/// item verschieben  //
	 	item_shape.y=dropnode.y; 																																			 	///									 //
	 	item_png.x=dropnode.x;   																																				///                   //
	 	item_png.y=dropnode.y;  																																				///                   //
	 	add_to_knapsack(item_shape); 																																		//item dem rucksack hinzufuegen
	 	item_shape.dropnode_index = dropnode.index;																											//aufenthaltsort/"mittiger dropnode" als index im item speichern, noetig fuer das loeschen
	}
}

function calc_item_star_pos(radius, angle)																													//Hilfsfunktion
{ 	this.x = cosinus(angle)*radius;
		this.y = sinus(angle)*radius;
}

function items_distribute(item_picture_array, stage)
{	set_coords_item_distribution();
	reset_idle();
	for(i=0; i < item_picture_array.length; i++)
	{ var okay = true;
		var random_index;
		while(okay==true)
		{ if(i%2==0) random_index = Math.floor(Math.random()*((item_coords_distr_a.length)/2));
			else random_index = Math.floor(Math.random()*((item_coords_distr_a.length)/2)+(item_coords_distr_a.length)/2);
			if (item_coords_distr_a[random_index].x!=-1) okay=false;
		}
		item_picture_array[i].homeX = item_coords_distr_a[random_index].x;
		item_picture_array[i].homeY = item_coords_distr_a[random_index].y;
		item_picture_array[i].x = item_picture_array[i].homeX + item_picture_array[i].getBounds().width/2;
		item_picture_array[i].y = item_picture_array[i].homeY + item_picture_array[i].getBounds().height/2;
		stage.addChild(item_picture_array[i]);
		item_coords_distr_a[random_index].x=-1; 																												//ausweich-wert
	}
	set_coords_item_distribution(); 																																	//ausweich-wert zuruecksetzen
}

function draw_shape(x_pos, y_pos, radius_small, radius_big, start_angle, end_angle, color,edge_color, edge_thickness, alpha, shape_array, color_array, stage, sterne_v, volumen_v)
{   reset_idle();
    var start_rad = Math.PI * start_angle/180;
    var end_rad = Math.PI * end_angle/180;
    var shape = new createjs.Shape().set({x:x_pos, y:y_pos}); 																			//neue shape verschoben auf x/y
    shape.graphics.beginStroke((createjs.Graphics.getRGB(255,140,0,0.9))).setStrokeStyle(edge_thickness);

    var color_tmp = shape.graphics.beginFill(color).command;
    shape.graphics.arc(0, 0, radius_small, start_rad, end_rad, false);
    shape.graphics.arc(0, 0, radius_big, end_rad, start_rad, true);

    shape.shape_center = get_shape_center(x_pos, y_pos, radius_small, radius_big, start_angle, end_angle);
    var rel_pos_ = 1.5;

    var _star_angle_ = 0;
    var _star_p_size = 0.6;
    var _star_radius = 30;
    var _star_spikes = 5;

    var _star_degree_center = 0.5*volumen_v*30;																								// mittlere achse des items (schneidet mittelpunkt der shape)
    var _fix = 7.5;
    var _one 		= new calc_item_star_pos( (7/6)*radius_big  	,_star_degree_center);					//moegliche positionen=koordinaten von sternen auf den items
    var _two_1 = new calc_item_star_pos(  (7/6)*radius_big  	,_star_degree_center+ _fix);		//+- fix um code zu sparen
    var _two_2 = new calc_item_star_pos(  (7/6)*radius_big  	,_star_degree_center- _fix);
    var _four_1 = new calc_item_star_pos( (11/6)*radius_big  	,_star_degree_center+ _fix);
    var _four_2 = new calc_item_star_pos( (11/6)*radius_big  	,_star_degree_center- _fix);
    var _five  = new calc_item_star_pos(  (11/6)*radius_big  	,_star_degree_center);
    shape.graphics.beginStroke((createjs.Graphics.getRGB(255,140,0,0.9))).setStrokeStyle(3);

    if(sterne_v==1)																																						//positionen f�llen, hier nur ein stern
		shape.graphics.beginFill("red").drawPolyStar(_one.x,_one.y,  _star_radius, _star_spikes, _star_p_size, _star_angle_);
		if(sterne_v>1) 																																						//bei 2 oder mehr sternen werden position _two_1 und _two_2 gefuellt
		{ shape.graphics.beginFill("red").drawPolyStar(_two_1.x,_two_1.y,  _star_radius, _star_spikes, _star_p_size, _star_angle_);
			shape.graphics.beginFill("red").drawPolyStar(_two_2.x,_two_2.y,  _star_radius, _star_spikes, _star_p_size, _star_angle_);
		}
		if(sterne_v==3||sterne_v==5)																															//bei 3 oder 5 sternen wird stern an position _five erzeugt
		shape.graphics.beginFill("red").drawPolyStar(_five.x,_five.y,  _star_radius, _star_spikes, _star_p_size, _star_angle_);
		if(sterne_v==4 || sterne_v==5)
		{	shape.graphics.beginFill("red").drawPolyStar(_four_1.x,_four_1.y,  _star_radius, _star_spikes, _star_p_size, _star_angle_);
			shape.graphics.beginFill("red").drawPolyStar(_four_2.x,_four_2.y,  _star_radius, _star_spikes, _star_p_size, _star_angle_);
		}
    shape.graphics.closePath();
    shape.alpha = alpha;

    shape.radius_small = radius_small;
    shape.radius_big = radius_big;
    shape.start_angle = start_angle;
    shape.end_angle = end_angle;
    stage.addChild(shape);
    shape_array.push(shape);
    color_array.push(color_tmp);
}

function get_shape_center(x, y, radius_small, radius_big, angle_beginning, angle_ending)
{   var result_angle = conv_to_rad((angle_beginning + angle_ending)/2);
    var distance = (radius_small+radius_big)/2;
    var x_distance = Math.cos(result_angle) * distance;
    var y_distance = Math.sin(result_angle) * distance;
    var result = new createjs.Point(x + x_distance, y_distance + y);
    return result;
}

function shape_move_with_picture(item_shape,item_png,arc_center_x,arc_center_y,shape_angle,png_angle,stage,ID,sterne_v,volumen_v)
{   reset_idle();																																						//neustart-button hinauszoegern
		if(ID > scenario_all_merged_array[scenario_type-1][scenario_index].vol_array.length-1)	//nur so viele items erzeugen, wie gewichtungen existieren
		{ stage.removeChild(item_png);
			stage.removeChild(item_shape);
		}

		else
		{	item_shape.regX = item_shape.shape_center.x - item_shape.x;
	    item_shape.regY = item_shape.shape_center.y - item_shape.y;
	    item_shape.ID = ID;
	    item_shape.sterne = sterne_v;
	    item_shape.volumen=volumen_v;
	    item_shape.dropnode_index;
	    item_shape.inside=false;

			item_shape.on("mousedown", function (evt)
			{
			reset_idle();
			if (item_shape.inside) remove_from_knapsack(item_shape);
			item_png.offset = {x: item_png.x - evt.stageX, y: item_png.y - evt.stageY};
			stage.setChildIndex(item_shape, stage.getNumChildren() - 2); 								// gro�er index <-> hoeherer layer
			stage.setChildIndex(item_png, stage.getNumChildren() - 1);
			});


			item_png.on("mousedown", function (evt)
			{	reset_idle();
			if (item_shape.inside) remove_from_knapsack(item_shape);

			item_png.offset = {x: item_png.x - evt.stageX, y: item_png.y - evt.stageY}; //einmalig offset setzen
			stage.setChildIndex(item_shape, stage.getNumChildren() - 2); 								//zu obersten layer erhoehen
			stage.setChildIndex(item_png, stage.getNumChildren() - 1);
			});

	    var angle_offset, alpha, alpha_degree, x_difference, y_difference;

			item_shape.on("pressmove", function (evt) {
			reset_idle();

			item_png.x = evt.stageX + item_png.offset.x;																// position des bildes aktualisieren bei touch
			item_png.y = evt.stageY + item_png.offset.y;

			x_difference = item_shape.x - arc_center_x;																	//differenz der Koordinanten
			y_difference = arc_center_y - item_shape.y;

			alpha = Math.atan(x_difference / y_difference);
			alpha_degree = conv_to_deg(alpha);
	    angle_offset = get_angle_offset(alpha_degree, x_difference,y_difference);

			item_shape.rotation = 180 + shape_angle + angle_offset;										// eigentliche rotation
			item_png.rotation = 180 + png_angle + angle_offset;

			item_shape.x = item_png.x;																								//koordinaten von bild und shape überstimmen
			item_shape.y = item_png.y;
		});


			item_png.on("pressmove", function (evt)
			{	reset_idle();
				item_png.x = evt.stageX + item_png.offset.x;
				item_png.y = evt.stageY + item_png.offset.y;
				x_difference = item_shape.x - arc_center_x;
				y_difference = arc_center_y - item_shape.y;
				alpha = Math.atan(x_difference / y_difference);
				alpha_degree = conv_to_deg(alpha);
		    angle_offset = get_angle_offset(alpha_degree, x_difference,y_difference);
				item_shape.rotation = 180 + shape_angle + angle_offset;							// eigentliche rotation
				item_png.rotation = 180 + png_angle + angle_offset;
				item_shape.x = item_png.x;
				item_shape.y = item_png.y;
			});


				item_shape.on("pressup", function (evt)
				{	reset_idle();
					var aiming = 2*calc_distance(dropnodes_a[0].x,dropnodes_a[0].y,dropnodes_a[1].x,dropnodes_a[1].y);
					if(calc_distance(item_shape.x,item_shape.y,center_c.x,center_c.y)<r_out*1.25)
					{		var target_index = nearest_node_value(	item_shape.x,	item_shape.y);
							if(target_index >-1){
								if(calc_distance(dropnodes_a[target_index].x,dropnodes_a[target_index].y,item_shape.x,item_shape.y)<aiming)
								{	nearest_node_to_place_value(item_shape,item_png,target_index,item_shape.volumen);
								}
							}
					x_difference = item_shape.x - arc_center_x;
					y_difference = arc_center_y - item_shape.y;
					alpha = Math.atan(x_difference / y_difference);
					alpha_degree = conv_to_deg(alpha);
			    angle_offset = get_angle_offset(alpha_degree, x_difference,y_difference);

					item_shape.rotation = 180 + shape_angle + angle_offset;														// eigentliche rotation
					item_png.rotation = 180 + png_angle + angle_offset;
							}
							else {remove_from_knapsack(item_shape);}
						draw_dropnodes();
				});

				item_png.on("pressup", function (evt)
				{	reset_idle();
					var aiming = 2*calc_distance(dropnodes_a[0].x,dropnodes_a[0].y,dropnodes_a[1].x,dropnodes_a[1].y);
					if(calc_distance(item_shape.x,item_shape.y,center_c.x,center_c.y)<r_out*1.25)
					{		var target_index_ = nearest_node_value(	item_shape.x,	item_shape.y);
							if(target_index_ >-1)
							{
								if(calc_distance(dropnodes_a[target_index_].x,dropnodes_a[target_index_].y,item_shape.x,item_shape.y)<aiming)
								{	nearest_node_to_place_value(item_shape,item_png,target_index_,item_shape.volumen);
								}
							}
							x_difference = item_shape.x - arc_center_x;
							y_difference = arc_center_y - item_shape.y;
							alpha = Math.atan(x_difference / y_difference);
							alpha_degree = conv_to_deg(alpha);
					    angle_offset = get_angle_offset(alpha_degree, x_difference,y_difference);

							item_shape.rotation = 180 + shape_angle + angle_offset; 										// eigentliche rotation
							item_png.rotation = 180 + png_angle + angle_offset;
					}
						else {remove_from_knapsack(item_shape);}
					draw_dropnodes();
				});

	    x_difference = item_png.x - arc_center_x;																						//einheitlichen bezugspunkt ermoeglichen
	    y_difference = arc_center_y - item_png.y;
	    alpha = Math.atan(x_difference / y_difference);
	    alpha_degree = conv_to_deg(alpha);
	    angle_offset = get_angle_offset(alpha_degree, x_difference,y_difference);


	    item_shape.rotation = 180 + shape_angle + angle_offset;															// eigentliche rotation
	    item_png.rotation = 180 + png_angle + angle_offset;

	    item_shape.x = item_png.x;
	    item_shape.y = item_png.y;

	    stage.setChildIndex(item_shape, stage.getNumChildren() - 2);   										 //shape zum 2. obersten layer
	    stage.setChildIndex(item_png, stage.getNumChildren() - 1); 												 //item zum obersten layer, also ueber der shape
	  }
}

function get_angle_offset(alpha_degree, x_difference,y_difference)
{ if(x_difference >= 0 && y_difference >= 0) 																						//oben rechts
	{	return alpha_degree;
	}
	if(x_difference >= 0 && y_difference < 0)																							//unten rechts
	{	return alpha_degree + 180;
	}
	if(x_difference < 0 && y_difference >= 0)																							//oben links
	{	return alpha_degree + 360;
	}
	if(x_difference < 0 && y_difference < 0)																							//unten links
	{	return  alpha_degree + 180;
	}
}
