//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//  BOARD ~ Variablen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var check_container = new createjs.Container();																																// Prüfen-Button
var angel = new createjs.Bitmap('img/spieler/fee.png');																														// Hier wird gemeint, dass der Angel wie einer Joker, der bei Erfindung der L�sung hilft.	
var number_click_check = 0;																																										// z�hlt die Anzahl des Klick von Prüfen-Button
var	need_angel_boolean =false;
var level_info_line = new createjs.Shape();
var in_game=false;																																														//es checkt, ob wir im Spiel oder im Feedback_Modus sind.
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//  BOARD ~ Hintergrund
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function draw_background_game(number_of_players, stage, background_part, background_color)
{
			var current_area,this_color;
			var row = 2;
			var columm = 2;

			var area_width = Math.floor(screen.width / columm);
			var  area_heigth = Math.floor(screen.height / row);

			for(i=0; i < row; i++){																																									// zeichnen die Rechtecke
				for(j=0; j < columm; j++){
					current_area = new createjs.Shape();																																//erzeuge Feld in Zeile i und Spalte j
					this_color = current_area.graphics.beginFill('white').command; 																			//this_color wird zuerst mit wei� gespeichert und durch ".command"  wird später mit style neue Farbe gespeichert werden
					background_color.push(this_color);																																	//fuege this_color dem background_color Array hinzu
					current_area.graphics.drawRect(j*area_width, i*area_heigth, area_width, area_heigth);								//zeichne Rechteck
					current_area.alpha = 0.3;
					stage.addChild(current_area);
					background_part.push(current_area);    																															//fuege Rechteck dem Hintergrundrechtecke-Array hinzu
				}
			}
			var player_width = 150;
			var player_height = 191;
			switch(number_of_players){    																																						//fürbe Rechtecke ein und zeichnen den Spielfigur
				case 1:																																																// Nur 1 Spieler
				var who_play;
				for(i=0;i<4;i++)
				{ if (player[i]!=0)
					who_play=player[i];
				  switch(who_play)
					{
						case 1:
						player1.x = 0;
						player1.y = 0;
						player1.alpha=0.7;
						stage.addChild(player1);
						break;
						case 2:
						player2.x = 0;
						player2.y = 0;
						player2.alpha=0.7;
						stage.addChild(player2);
						break;
						case 3:
						player3.x = 0;
						player3.y = 0;
						player3.alpha=0.7;
						stage.addChild(player3);
						break;
						case 5:
						player4.x = 0;
						player4.y = 0;
						player4.alpha=0.7;
						stage.addChild(player4);
						break;
					}
					background_color[0].style='red';																																		// background_color des allen Viertels wird auf rot gesetzt
					background_color[1].style='red';
					background_color[2].style='red';
					background_color[3].style='red';
				}
				break;

				case 2: 																																															// 2 Spieler & die Spielfiguren werden auch nach Position rotiert
						var sum =0;
						for(i=0;i<4;i++)
						{
							sum=sum+player[i];																																							// Spieler 1 hat Nr 1, 2 hat Nr2, 3 hat Nr 3, 4 hat Nr 5
						}
						switch(sum){
							case 3:   																																											// Spieler 1 & 2
										player1.x = player_height;
										player1.y = 0;
										player1.alpha=0.7;
										player1.rotation=90;
										stage.addChild(player1);
										player2.x = screen.width-player_height;
										player2.y = player_width;
										player2.rotation=-90;
										player2.alpha=0.7;
										stage.addChild(player2);
							break;
							case 4:																																													// Spieler 1 & 3
										player1.x = player_height;
										player1.y = 0;
										player1.alpha=0.7;
										player1.rotation=90;
										stage.addChild(player1);
										player3.x = screen.width-player_height;
										player3.y = player_width;
										player3.alpha=0.7;
										player3.rotation=-90;
										stage.addChild(player3);
							break;
							case 6:																																													// Spieler 1 & 4
										player1.x = player_height;
										player1.y = 0;
										player1.alpha=0.7;
										player1.rotation=90;
										stage.addChild(player1);
										player4.x = screen.width-player_height;
										player4.y = player_width;
										player4.alpha=0.7;
										player4.rotation=-90;
										stage.addChild(player4);
							break;
							case 5:																																													// Spieler 2 & 3
										player2.x = player_height;
										player2.y = 0;
										player2.alpha=0.7;
										player2.rotation=90;
										stage.addChild(player2);
										player3.x = screen.width-player_height;
										player3.y = player_width;
										player3.alpha=0.7;
										player3.rotation=-90;
										stage.addChild(player3);

							break;
							case 7:																																													// Spieler 2 & 4
										player2.x = player_height;
										player2.y = 0;
										player2.alpha=0.7;
										player2.rotation=90;
										stage.addChild(player2);
										player4.x = screen.width-player_height;
										player4.y = player_width;
										player4.alpha=0.7;
										player4.rotation=-90;
										stage.addChild(player4);

							break;
							case 8:																																													// Spieler 3 & 4
										player3.x = player_height;
										player3.y = 0;
										player3.alpha=0.7;
										player3.rotation=90;
										stage.addChild(player3);
										player4.x = screen.width-player_height;
										player4.y = player_width;
										player4.alpha=0.7;
										player4.rotation=-90;
										stage.addChild(player4);

							break;
						}
						background_color[1].style='blue';																																	// background_color des rechten Viertels wird auf blau gesetzt
						background_color[3].style='blue';

						background_color[0].style='red';																																	// background_color des linken Viertels wird auf rot gesetzt
						background_color[2].style='red';

				break;

				case 3: 																																															//3 Spieler
							var sum=0;
							for ( i=0;i<4;i++)
							{
								sum=sum+player[i];
							}

							switch(sum){
								case 6:																																												// Spieler 1 & 2 & 3
											player1.x = player_width;
											player1.y = player_height;
											player1.alpha=0.7;
											player1.rotation=180;
											stage.addChild(player1);
											player2.x = screen.width;
											player2.y = player_height;
											player2.alpha=0.7;
											player2.rotation=180;
											stage.addChild(player2);
											player3.x = 0;
											player3.y = screen.height-player_height;
											player3.alpha=0.7;
											stage.addChild(player3);
								break;

								case 8:																																												// Spieler 1 & 2 & 4
											player1.x = player_width;
											player1.y = player_height;
											player1.alpha=0.7;
											player1.rotation=180;
											stage.addChild(player1);
											player2.x = screen.width;
											player2.y = player_height;
											player2.alpha=0.7;
											player2.rotation=180;
											stage.addChild(player2);
											player4.x = 0;
											player4.y = screen.height-player_height;
											player4.alpha=0.7;
											stage.addChild(player4);

								break;

								case 9:																																												// Spieler 1 & 3 & 4
											player1.x = player_width;
											player1.y = player_height;
											player1.alpha=0.7;
											player1.rotation=180;
											stage.addChild(player1);
											player3.x = screen.width;
											player3.y = player_height;
											player3.alpha=0.7;
											player3.rotation=180;
											stage.addChild(player3);
											player4.x = 0;
											player4.y = screen.height-player_height;
											player4.alpha=0.7;
											stage.addChild(player4);

								break;
								case 10:																																											// Spieler 2 & 3 & 4
										player2.x = player_width;
										player2.y = player_height;
										player2.alpha=0.7;
										player2.rotation=180;
										stage.addChild(player2);
										player3.x = screen.width;
										player3.y = player_height;
										player3.alpha=0.7;
										player3.rotation=180;
										stage.addChild(player3);
										player4.x = 0;
										player4.y = screen.height-player_height;
										player4.alpha=0.7;
										stage.addChild(player4);
								break;
							}
							background_color[1].style='blue';																																//blau oben rechts

							background_color[0].style='red';																																//rot unten links

							background_color[2].style='yellow';																															//geld unten rechts
																																																							// die restlichen Teile bleiben weiss
				break;

				case 4:
								player1.x = player_width;
								player1.y = player_height;
								player1.alpha=0.7;
								player1.rotation=180;
								stage.addChild(player1);
								player2.x = screen.width;
								player2.y = player_height;
								player2.alpha=0.7;
								player2.rotation=180;
								stage.addChild(player2);
								player3.x = 0;
								player3.y = screen.height-player_height;
								player3.alpha=0.7;
								stage.addChild(player3);
								player4.x = screen.width-player_width;
								player4.y = screen.height-player_height;
								player4.alpha=0.7;
								stage.addChild(player4);
																																																							// faerbe fuer jeden Viertel eine Farbe
						background_color[0].style='blue';
						background_color[1].style='red';
						background_color[2].style='#EB94E0';
						background_color[3].style='yellow';
				break;

			}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//  ANGEL
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


		  var ufo_board= new createjs.Bitmap('img/spieler/ufo_board.png')
		  ufo_board.x=screen.width/2-360;
		  ufo_board.y=-200;
		  stage.addChild(ufo_board);


		  angel.x=screen.width/2-50;
			angel.y=0;
			stage.addChild(angel);
			angel.visible = false;
			angel.addEventListener("click",click_angel);
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//  BOARD ~ Button
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function check_button_init()																																									//Wird Check_Button benutzt,um zu prüfen, ob die max.Anzahl der Sterne erreicht oder nicht und dadurch man in n�chstes Spiel kommen kann
{	var check = new createjs.Shape();
	check.graphics.beginFill(createjs.Graphics.getRGB(255,140,0,0.9)).drawRoundRect(0,0, 180, 100,20);
	var textCheck = new createjs.Text("Prüfen", "bold 44px Gabriola", "#FFFFFF");
	textCheck.textAlign = "center";
	textCheck.textBaseline = "middle";
	textCheck.x = 180/2;
	textCheck.y = 100/2;


	check_container.x = s5b_c.x/2-90 ;
	check_container.y = s5b_c.y-100;

	check_container.addChild(check, textCheck);
	check_container.visible=false;																																								//am Anfang ist der Button nicht siehbar, man kann nur den Knopf sehen wenn mind. ein Item in Rucksack eingezogen wird
	check_container.alpha=0.8;
	stage.addChild(check_container);

	check_container.addEventListener("click", click_check);

	stage.update()
}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//  Event-Funktion
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function click_check(event)
{
		update_dropnodes();
		number_click_check ++ ;
		if(star_quantity>0)
		{	in_feedbackmenu_boolean = true;
			stage.alpha=1;

			for(i=0;i<item_shapes_a.length;i++)																																			//alle Shapes werden versteckt
					item_shapes_a[i].visible=false;

			for(i=0;i<item_picture_a.length;i++)																																		//alle Item-Bilder werden auch vesteckt
					item_picture_a[i].visible=false;

			check_container.visible=false;
			check_game(stage);
		}

}


function click_angel(event)
{			var not_opt_picture_array= new Array();
			var not_opt_shapes_array=new Array();
			angel.visible=false;
																																																							// Fee/Bombe Implementierungsbeginn
}



function level_information_horizontal(actual_level, max_level)																								//zeigen in welchem Level sind wir durch die gefürbte Linie.
{ in_game=true;
	level_info_line.graphics.clear;
	var margine_tmp = 4;																																												//Abstand zum vertikalen Rand
	var target_x = (screen.width-2*margine_tmp)*(actual_level+1)/max_level;
	var always_y = screen.height-margine_tmp;
	level_info_line.graphics.beginStroke("orange",0.8);
	level_info_line.graphics.setStrokeStyle(margine_tmp,"square",0,false);
	level_info_line.graphics.moveTo(margine_tmp,always_y).lineTo(target_x,always_y);
	level_info_line.graphics.beginStroke("DimGrey",0.2);
	level_info_line.graphics.moveTo(target_x,always_y).lineTo(screen.width-2*margine_tmp,always_y);
	stage.addChild(level_info_line);

}
