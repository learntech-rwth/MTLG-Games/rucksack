//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// HILFS-FUNKTIONALITAETEN ~ Variablen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var s5b_c = new coords(screen.width, screen.height ); 								//koords der rechten unteren ecke
const center_c = new coords(s5b_c.x/2,s5b_c.y/2);         						//koords am zentrum des bildschirms
var allow_alerts = false;
var interrupt_intervall = 50; 																				//200 FPS
var seconds_to_wait_idle = 90; 																				//sekunden bis man das spiel neu laden kann
var time_to_reset_coundown = seconds_to_wait_idle*1000/interrupt_intervall;
var time_to_reset_startvalue = time_to_reset_coundown;
var is_there_a_reboot_button = false;																	//hilfvariable, check ob wir im Warte-Modus sind

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// HILFS-FUNKTIONALITAETEN ~ Funktionen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function reset_idle()																									//timer vom neustart-button wieder auf anfangswert setzen
{	time_to_reset_coundown = time_to_reset_startvalue;
}

function check_idle()																									//test ob: "wurde lange nicht mit dem spiel interagiert?"
{ if (time_to_reset_coundown > 0) time_to_reset_coundown--;						//timer vom neustart button ist noch nicht lange genug gelaufen
	else
	{	reset_idle();																											//timer vom neustart-button wieder auf anfangswert setzen
		wait_board();																											//neustart button erzeugen um neuankommenden spielern, die moeglichkeit
																																			//zu geben, das spiel neu zu beginnen
	}
}

function show_pictures()																							//Alterniert zwischen einem Null-Wert und einer Funktion, die einen Ticker erzeugt. Die Funktion ist dazu da, die Bilder explizit anzeigen zu lassen
{
	return;
	createjs.Ticker.interval=interrupt_intervall;
	createjs.Ticker.addEventListener("tick", function(e){
 	stage.update();
 	});
 	show_pictures=0;
}

function wait_board()																															// warte_modus wenn man nach lange Zeit gar nichts macht
{	if(is_there_a_reboot_button) NOOP();
	else																																						//nicht in Warte_modus
	{	var wait_background = new createjs.Shape(); 																	//einen Background für Warte_Modus erzeugen
		wait_background.graphics.beginFill("#123456").drawRect(0, 0, screen.width, screen.height);
		wait_background.alpha=0.75;
		stage.addChild(wait_background);
		createjs.Touch.enable(stage);																									//kann auf dem Display klicken



		var reboot = new createjs.Shape();																						//	restart-button erzeugen
		reboot.graphics.beginFill("orange").drawRoundRect(0,0, 150, 65,16);
		var reboot_text = new createjs.Text("  Neustart", "bold 28px Gabriola", "#FFFFFF");
		reboot_text.textBaseline = "middle";
		reboot_text.x = 20;
		reboot_text.y = 65/2;
		var reboot_container = new createjs.Container();
		reboot_container.x = screen.width/2-75 ;
		reboot_container.y = screen.height/2;
		reboot_container.addChild(reboot, reboot_text);
		reboot_container.alpha=0.6;
		stage.addChild(reboot_container);

		wait_background.addEventListener("click", function (evt){											//irgendwo auf dem Display klick
				stage.removeChild(wait_background);																				//Warte_modus löschen
				stage.removeChild(reboot_container);																			//restart_button löschen
				reset_idle();																															//Timer reset
				is_there_a_reboot_button = false;
		});
		reboot_container.addEventListener("click",function(e){												//auf Restart_button klick
				location.reload();																												//wie F5

		});
	is_there_a_reboot_button = true;
	}

}



function fullscreen()																															//automatischer fullscreenmodus bei mozilla firefox
{
	if(stage.webkitRequestFullScreen)
	{	stage.webkitRequestFullScreen();
	}
	else
	{	stage.mozRequestFullScreen();
	}
}

function ticker_start()																														//ticker mit jeweils 50ms (interrupt_intervall=50) zwischen den interrupts
{	createjs.Ticker.interval=interrupt_intervall;
	createjs.Ticker.addEventListener("tick", function(e){
 	check_idle();																																		//countdown wird dekrementiert (wenn zu lange keine interaktion vorkam, erscheint neustart button)
 	});
}

function calc_segment_center(x1,y1,x2,y2) 																				//vergleiche mit calc_abstand?
{	temp_segment_center.x=(x1+x2)*0.5;
	temp_segment_center.y=(y1+y2)*0.5;
}

function conv_to_deg(radians) 																										//radiant->grad
{	return (radians * 180.0 / Math.PI);
}

function conv_to_rad(degrees) 																										//grad->radiant
{ return (degrees * Math.PI / 180.0);
}

function cosinus(c)  																															//grad als parameter
{	return (Math.cos(conv_to_rad(c)));
}

function sinus(s) 																																//grad als parameter
{ return (Math.sin(conv_to_rad(s)));
}

function atang(a)																																	//radiant als parameter
{ return (Math.atan(conv_to_deg(a)));
}


function NOOP(){} 																																//'no operation'

function calc_distance(x,y,xx,yy) 																								//abstand von (x,y) zu (xx,yy)
{ return (Math.sqrt(Math.pow(x-xx,2)+Math.pow(y-yy,2)))
}

function coords(x,y)  																														//datentyp fuer koordinaten
{ this.x = x;
	this.y = y;
}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// HILFSFUNKTIONEN ~ Feedback
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function refresh_all()																														//variablereset fuer die click-methoden (next, restart...)
{
	star_quantity=0;																																//anzahl der erreichbare Sterne wieder auf 0
	items_within_knapsack= new Array();																							//item-in-rucksack-array leeren
	background_part = new Array();																									//background-array leeren
	background_color = new Array();																									//background_color leeren
	turning_stars_a = new Array();
	item_shapes_a=new Array();  																										//item_shapes_array leeren
	item_shapes_color=new Array(); 																									//item_shape_color_array leeren
	number_of_bad_try =0;
	number_click_check=0;

}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// HILFSFUNKTIONEN ~ Menu # Pressmove_Event
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function move(event)																															//Mausklick ist auf Mitte des Bildes
{		event.currentTarget.x = event.stageX -player_width/2;
		event.currentTarget.y = event.stageY -player_height/2;
		stage.update();
}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// HILFSFUNKTIONEN ~ Menu # Shape auf Shape ziehen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function intersect(draggable, box) 																								//vom minimalbeispiel uebernommen (ziehen auf einer box)
{	var draggableBounds = draggable.getBounds().clone();
	var boxBounds = box.getBounds().clone();
																																									// Berechnet ausgehend von der x-/y-Koordinate im Bezug zur stage die lokalen
																																									// x-/y-Koordinaten im Bezug zum entsprechenden Objekt.
	var point = draggable.globalToLocal(boxBounds.x, boxBounds.y);
	if(point.x > (boxBounds.width / 2) || point.x < -(draggableBounds.width / 2 + boxBounds.width)) {
		return false;
	}
	if(point.y > (draggableBounds.height / 2) || point.y < -(draggableBounds.height / 2 + boxBounds.height)) {
		return false;
	}
	return true;
}


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// HILFSFUNKTIONEN ~ Debugging & Testing
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function listdropnodes24() 																												//debugging-hilfe
{	alert(
	"x: "+dropnodes_a[0 ].x+"y: "+dropnodes_a[0 ].y+"state: "+dropnodes_a[0 ].state+"\n "+
	"x: "+dropnodes_a[1 ].x+"y: "+dropnodes_a[1 ].y+"state: "+dropnodes_a[1 ].state+"\n "+
	"x: "+dropnodes_a[2 ].x+"y: "+dropnodes_a[2 ].y+"state: "+dropnodes_a[2 ].state+"\n "+
	"x: "+dropnodes_a[3 ].x+"y: "+dropnodes_a[3 ].y+"state: "+dropnodes_a[3 ].state+"\n "+
	"x: "+dropnodes_a[4 ].x+"y: "+dropnodes_a[4 ].y+"state: "+dropnodes_a[4 ].state+"\n "+
	"x: "+dropnodes_a[5 ].x+"y: "+dropnodes_a[5 ].y+"state: "+dropnodes_a[5 ].state+"\n "+
	"x: "+dropnodes_a[6 ].x+"y: "+dropnodes_a[6 ].y+"state: "+dropnodes_a[6 ].state+"\n "+
	"x: "+dropnodes_a[7 ].x+"y: "+dropnodes_a[7 ].y+"state: "+dropnodes_a[7 ].state+"\n "+
	"x: "+dropnodes_a[8 ].x+"y: "+dropnodes_a[8 ].y+"state: "+dropnodes_a[8 ].state+"\n "+
	"x: "+dropnodes_a[9 ].x+"y: "+dropnodes_a[9 ].y+"state: "+dropnodes_a[9 ].state+"\n "+
	"x: "+dropnodes_a[10].x+"y: "+dropnodes_a[10].y+"state: "+dropnodes_a[10].state+"\n "+
	"x: "+dropnodes_a[11].x+"y: "+dropnodes_a[11].y+"state: "+dropnodes_a[11].state+"\n "+
	"x: "+dropnodes_a[12].x+"y: "+dropnodes_a[12].y+"state: "+dropnodes_a[12].state+"\n "+
	"x: "+dropnodes_a[13].x+"y: "+dropnodes_a[13].y+"state: "+dropnodes_a[13].state+"\n "+
	"x: "+dropnodes_a[14].x+"y: "+dropnodes_a[14].y+"state: "+dropnodes_a[14].state+"\n "+
	"x: "+dropnodes_a[15].x+"y: "+dropnodes_a[15].y+"state: "+dropnodes_a[15].state+"\n "+
	"x: "+dropnodes_a[16].x+"y: "+dropnodes_a[16].y+"state: "+dropnodes_a[16].state+"\n "+
	"x: "+dropnodes_a[17].x+"y: "+dropnodes_a[17].y+"state: "+dropnodes_a[17].state+"\n "+
	"x: "+dropnodes_a[18].x+"y: "+dropnodes_a[18].y+"state: "+dropnodes_a[18].state+"\n "+
	"x: "+dropnodes_a[19].x+"y: "+dropnodes_a[19].y+"state: "+dropnodes_a[19].state+"\n "+
	"x: "+dropnodes_a[20].x+"y: "+dropnodes_a[20].y+"state: "+dropnodes_a[20].state+"\n "+
	"x: "+dropnodes_a[21].x+"y: "+dropnodes_a[21].y+"state: "+dropnodes_a[21].state+"\n "+
	"x: "+dropnodes_a[22].x+"y: "+dropnodes_a[22].y+"state: "+dropnodes_a[22].state+"\n "+
	"x: "+dropnodes_a[23].x+"y: "+dropnodes_a[23].y+"state: "+dropnodes_a[23].state)
}

function listitemcoords24() 																											//debugging-hilfe
{	if (allow_alerts) alert(
	"x: "+item_coords_distr_a[0 ].x+"y: "+item_coords_distr_a[0 ].y+"\n "+
	"x: "+item_coords_distr_a[1 ].x+"y: "+item_coords_distr_a[1 ].y+"\n "+
	"x: "+item_coords_distr_a[2 ].x+"y: "+item_coords_distr_a[2 ].y+"\n "+
	"x: "+item_coords_distr_a[3 ].x+"y: "+item_coords_distr_a[3 ].y+"\n "+
	"x: "+item_coords_distr_a[4 ].x+"y: "+item_coords_distr_a[4 ].y+"\n "+
	"x: "+item_coords_distr_a[5 ].x+"y: "+item_coords_distr_a[5 ].y+"\n "+
	"x: "+item_coords_distr_a[6 ].x+"y: "+item_coords_distr_a[6 ].y+"\n "+
	"x: "+item_coords_distr_a[7 ].x+"y: "+item_coords_distr_a[7 ].y+"\n "+
	"x: "+item_coords_distr_a[8 ].x+"y: "+item_coords_distr_a[8 ].y+"\n "+
	"x: "+item_coords_distr_a[9 ].x+"y: "+item_coords_distr_a[9 ].y+"\n "+
	"x: "+item_coords_distr_a[10].x+"y: "+item_coords_distr_a[10].y+"\n "+
	"x: "+item_coords_distr_a[11].x+"y: "+item_coords_distr_a[11].y+"\n "+
	"x: "+item_coords_distr_a[12].x+"y: "+item_coords_distr_a[12].y+"\n "+
	"x: "+item_coords_distr_a[13].x+"y: "+item_coords_distr_a[13].y+"\n "+
	"x: "+item_coords_distr_a[14].x+"y: "+item_coords_distr_a[14].y+"\n "+
	"x: "+item_coords_distr_a[15].x+"y: "+item_coords_distr_a[15].y+"\n "+
	"x: "+item_coords_distr_a[16].x+"y: "+item_coords_distr_a[16].y+"\n "+
	"x: "+item_coords_distr_a[17].x+"y: "+item_coords_distr_a[17].y+"\n "+
	"x: "+item_coords_distr_a[18].x+"y: "+item_coords_distr_a[18].y+"\n "+
	"x: "+item_coords_distr_a[19].x+"y: "+item_coords_distr_a[19].y+"\n "+
	"x: "+item_coords_distr_a[20].x+"y: "+item_coords_distr_a[20].y+"\n "+
	"x: "+item_coords_distr_a[21].x+"y: "+item_coords_distr_a[21].y+"\n "+
	"x: "+item_coords_distr_a[22].x+"y: "+item_coords_distr_a[22].y+"\n "+
	"x: "+item_coords_distr_a[23].x+"y: "+item_coords_distr_a[23].y)
}
