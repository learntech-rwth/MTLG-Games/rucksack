var MTLG=(function(m){
  var deviceOptions = {
    "breite":1920, //Screen width in pixels
    "hoehe":1080, //height in pixels
    "zoll":27, //screen size in inches
    "mockLogin":true, //set to true to skip login procedure
    "mockNumber":2, //number of generated fake logins if mockLogin is true
    "loginMode":6, //Determines the login procedure for loginKinnut
    "language":"en", //Highest priority language setting, device dependant
  }

  m.loadOptions(deviceOptions);
  return m;
})(MTLG);
