var all_picture_names_1;
var all_picture_names_2;
var all_picture_names_3;
var all_picture_names_tmp;
var scenario_index = 3;
var border_thickness = 11;


var distance = 10;																																								// Standardabstand zum Rand

																																																	// globale Variablen
var stage;
var i; 																																														// zaehlvariable fuer alle aeu�eren Schleifen
var j; 																																														// zaehlvariable fuer alle inneren Schleifen

																																																	// globale nur intern genutzte Arrays
var _cache_array = []; 																																						// temporaeres Array für ausgelagerte Funktion
var item_shapes_a = []; 																																					// hilfsarray zur erstellung der Spielteile

																																																	//globale arrays
var background_part = new Array(); 																																// speichert die Rechtecke des Hintergrundes
var background_color = new Array(); 																															// speichert die Farbe der Rechtecke des Hintergrundes
var item_picture_a= new Array(); 																																	// picture array des Items
var item_shapes_color = new Array();																															// color der Shape


function spiel_start()
{ ticker_start();

	var ladetext = new createjs.Text("station5board", "300px Lucida", 'orange');
	ladetext.x = screen.width/2 - ladetext.getBounds().width/2;
	ladetext.y = screen.height/2;
	ladetext.textBaseline = "alphabetic";
  stage.addChild(ladetext);
  loadqueue_instance = new createjs.LoadQueue(true);
  loadqueue_instance.on("fileload", handle_manifest);
  loadqueue_instance.on("complete", draw_everything);

  loadqueue_instance.loadManifest(import_all_png(all_picture_names_tmp), true, "img/item_pictures/");	//Bilder mit den geschrieben Namen in Array aus der Datei

  item_picture_a = _cache_array;
  _cache_array.length=0;
}


function draw_everything()
{ reset_idle();
  stage.removeAllChildren();

  draw_background_game(number_of_players, stage, background_part, background_color);							//zeichne hintergrund vom spielfeld
  draw_knapsack();																																								//zeichne Rucksack
  set_dropnodes();																																								//zeichne dropnodes
  star_init();																																										//zeichne sterne
  check_button_init();																																						//erzeugen des Pruefen-buttons
  items_distribute(item_picture_a, stage);																												//items auf Spielfeld verteilen

  for(i=0; i < item_picture_a.length; i++)
	{	draw_shape(	screen.width/2, screen.height/2,r_out, r_in, 0,
							 	scenario_all_merged_array[scenario_type-1][scenario_index].vol_array[i]*conv_to_deg(slot_angle_),
		          	"green", "orange", border_thickness, 0.5,item_shapes_a, item_shapes_color,stage,
		        		scenario_all_merged_array[scenario_type-1][scenario_index].stars_array[i],
		        		scenario_all_merged_array[scenario_type-1][scenario_index].vol_array[i]);

		shape_move_with_picture(	item_shapes_a[i], item_picture_a[i], screen.width/2, screen.height/2,
															90-scenario_all_merged_array[scenario_type-1][scenario_index].vol_array[i]*conv_to_deg(slot_angle_)/2,
															0, stage,i,
															scenario_all_merged_array[scenario_type-1][scenario_index].stars_array[i],
															scenario_all_merged_array[scenario_type-1][scenario_index].vol_array[i]);

	}
	level_information_horizontal(scenario_index, 18);
	reset_idle();
}
