//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// RUCKSACK ~ Variablen 
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var slot_radius = r_out-0.5*r_in;																																		//radius für item-slots (gezeichnete dropzones)
var slot_quantity = 12*1;																																						//anzahl der item-slots im rucksack
var r_out = s5b_c.y/3;   																																						// radius aeusserer kreis = der kreis, der den rucksack umgibt 
var r_in = r_out/2;   																																							// 	radius innerer kreis = der kreis, der die sterne umgibt
var items_within_knapsack = new Array();

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// SLOTS ~ Variablen 
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var r_slot = r_out/3; 																																							//radius grosser kreis
var line_thickness_ = r_out/3; 																																			//liniendicke beim arc ist die dicke des arcs,standard ist ~1 pixel,
var bug_yellow = (slot_quantity/12) * 0.009; 																												//aus designgruenden kuenstlichen fehler einbauen, so kommt orange vom unteren layer besser durch
var slot_angle_ = conv_to_rad(360/slot_quantity); 																									//slot winkel einheit in radiant (arcs brauchen winkelangaben)

var slot_farbe = "DimGrey"; 																																				//farbe als variable zur besseren wartbarkeit/veraenderbarkeit

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// RUCKSACK ~ Funktionen # Zeichnen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function draw_knapsack() 																																						//zeichnet den rucksack (@Gruppe: name ungluecklich gewaehlt TODO)
{	shape_out = new createjs.Shape(); 																																// shapes fuer den rucksack
	shape_in = new createjs.Shape();
	stage.addChild(shape_out);																																				//zur stage hinzufuegen
	stage.addChild(shape_in);
	shape_out.graphics.beginFill("orange"); 																													// farben zuweisen
	shape_in.graphics.beginFill("DimGrey"); 
	
	shape_out.graphics.drawCircle(center_c.x,center_c.y,r_out);   																																						//grossen orangen kreis zeichnen (unterster layer)
  shape_in.graphics.drawCircle(center_c.x,center_c.y,r_in*0.99);																																						//kleineren kreis zeichnen *0.99 aus design-grunden, orange kommt am rand durch
  	
  draw_slots(); 																																										//item-slots zeichnen	  
}

function draw_slots() 																																							//item slots zeichnen
{	for(index =0; index < slot_quantity; index++)
	{	shape = new createjs.Shape().set({x:(center_c.x), y:(center_c.y)});														  //verschoben auf Bildschirmmitte
		stage.addChild(shape);
		shape.graphics.beginStroke(slot_farbe).setStrokeStyle(line_thickness_*1.5,0,0,false);						//farbe und liniendicke setzen
																																																		//bug_yellow aus design-gruenden, orange kommt besser durch
																																																		//parameter:     (x,y,radius        ,startAngle       ,endAngle , anticlockwise Number) 
		shape.graphics.arc(0,0,r_out-0.5*r_in,index*slot_angle_,index*slot_angle_+slot_angle_/(1+bug_yellow)); 
	
		stage.update(); 
	}
}

function add_to_knapsack(item) 																																			//item dem rucksack hinzufuegen
{	for(i=0;i<items_within_knapsack.length;i++) 																											//alle im rucksack enthaltenen items durchgehen
 	{	if(items_within_knapsack[i].ID==item.ID) return; 																								//falls item schon enthalten, nichts tun
	} 	
 	items_within_knapsack.push(item);																																	//ansonsten item hinzufuegen
 	item.inside=true;																																									///item bekommt noch signalisiert, dass es enthalten ist //
 											 																																							///  um seine eventlistener zu entlasten		  			//
 	update_sterne_value();																																						//items haben sterne, also sterne im rucksack aktuallisieren
}
 
function remove_from_knapsack(item) 																																//item aus dem rucksack entfernen, falls enthalten
{ empty_dropnodes(item.ID); 																																				//eventuellen besitz an dropnodes vom item aufheben 
	for(i=0;i<items_within_knapsack.length;i++) 																											//alle im rucksack enthaltenen items durchgehen
 	{ if(items_within_knapsack[i].ID==item.ID)  																											//item ist enthalten
 		{	items_within_knapsack.splice(i,1); 																														//item aus dem array der enthaltenden items entfernen
 			item.inside=false;																																						//fuer seine eventlisteners: item weiss es ist nicht mehr enthalten ist
 			item.dropnode_index = -1; 																																		//es besitzt keine dropnodes also ist index -1
 			update_sterne_value(); 																																				//item wurde entfernt, also sind weniger sterne im rucksack, muessen also sterne aktuallisieren
 			return; 																																											// diese funktion hat spaetestens hier ihre pflicht getan
 		}	
 	}
}
