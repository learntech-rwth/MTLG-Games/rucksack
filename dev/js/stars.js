



function update_sterne_value() 																				//sternen anzahl aktuallisieren inkl. drehgeschwindigkeit der sterne
{ star_quantity = 0; 																									//zaehler und globale variable nullen
	if (items_within_knapsack.length == 0) 
	{
		check_container.visible = false;
		return; 																													//kein item im rucksack -> keine sterne -> NOOP()
	}
	for(i=0;i<items_within_knapsack.length;i++) star_quantity +=  items_within_knapsack[i].sterne ; //aus items des rucksack auslesen&aufzaehlen
																				//drehgeschwindigkeit der sterne in abhaengigkeit anzahl der sterne
	if(scenario_index>12)turn_speed = star_quantity/2; 
	else turn_speed = star_quantity;
	check_container.visible = true;
	stage.update();		
}

function star_update() 																								//sternen-array neu fuellen, falls neue sternen-anzahl ermittelt
{	if(star_quantity==star_quantity_new) NOOP(); 												//nichts neues, nichts zu tun
	else 
	{	stars_a.length=0; 																								//sterne-array loeschen
		for (i = 0; i < star_coords_a.length; i++) 												//sternen-koordinaten durchgehen
		{	stars_a[i]=( createGraphics(star_coords_a[i].x,star_coords_a[i].y,i)); //sterne-array fuellen, jeweils eine shape, x und y
		}		
	}
}
	
function star_force_update() 																				  //quasi identisch zur star_update(), mit unterschied des forcierten befuellens
	{	for (i = 0; i < star_coords_a.length; i++) 												//sternen-koordinaten durchgehen
		{	stars_a[i]=( createGraphics(star_coords_a[i].x,star_coords_a[i].y,i));//sterne-array fuellen, jeweils eine shape, x und y
		}		
	}