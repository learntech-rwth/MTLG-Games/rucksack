
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// DROPNODES ~ Variablen inkl. Konstruktor
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
var dropnode_quantity = 2*slot_quantity;															//anzahl der dropnodes
var dropnodes_a = new Array();																				//alle dropnodes im array
for (i = 0; i < dropnode_quantity; i++) 															//das array gleich fuellen
{	dropnodes_a[i]= new create_dropnode();
}
var drawn_dropnodes_a = new Array();																	//gezeichnete dropnodes (sehr kleine scheiben)
		
function create_dropnode(x,y)																					//konstruktor fuer dropnodes
{ this.x = x;
	this.y = y;
	this.index = 0;																											//arrayindex fuer zugriff via dropnode selbst 
	this.ownerID=-1; 																										//ID des besitzenden items
	this.ownerID_=-1;																										//zweite ID bei 'kanten' zwischen zwei direkt benachbarten items
	this.state="frei";																									//dropnode-status kann "frei" XOR "halbfrei" XOR "nichtfrei" sein
} 
dropnodes_rel_pos = 0.75;																							//radius-faktor zur positionierung der dropnode koordinaten

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// DROPNODES  ~ Funktionen
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function set_dropnodes() 																							//koordinaten der dropnodes berechnen & in array speichern
{	var tmp_angle	=	0; 
	dropnode_quantity = 2*slot_quantity;
	const angle = 360/(dropnode_quantity);															//benutze grundeinheit in grad
	for (i = 0; i < dropnode_quantity; i++) 														//positionen der dropnodes bestimmen 
	{	var result = new create_dropnode(); 
		tmp_angle += angle; 																							//im uhrzeigersinn weiterlaufen
																																			//koordinaten (a,b): a=radius*sin(alpha)+x  
		result.x =  sinus(tmp_angle)*r_out*dropnodes_rel_pos + center_c.x;//sinus(winkel)*radius*radius_faktor vom zentrum aus
																																			//koordinaten (a,b): b=y-cos(alpha)*radius 
		result.y = center_c.y - cosinus(tmp_angle)*r_out*dropnodes_rel_pos;//-1*cos(winkel)*radius*radius_faktor vom zentrum aus
		result.index = i; 																							//index des dropnodes nochmal in variable innerhalb des dropnodes gespeichert fuer verbesserten zugriff
		dropnodes_a[i]=result; 																					//speichern des dropnodes im dropnodes-array
	} 																																	//alle positionen wurden bestimmt und gespeichert/gezeichnet
	draw_dropnodes(); 																									//dropnodes zeichnen
	if(dropnode_quantity!=dropnodes_a.length) ;//alert("ungleiche dropnodes#length");//fehlermeldung			
}
		
function draw_dropnodes() 																						//aktuellen status der dropnodes zeichnen (farbige punkte)
{	for (i = 0; i < dropnode_quantity; i++) 														//dropnodes durchgehen
	{	drawn_dropnodes_a[i] = new createjs.Shape(); 											//array aus shapes wird befuellt (shapes sind farbige punkte)
		var r=000;  																											//
		var g=000; 																												// variablen fuer rgb werte
		var b=000;																												//
		if(dropnodes_a[i] !== undefined) 																	//status der dropnodes farblich unterscheiden
		{	if (dropnodes_a[i].state == "nichtfrei")	{r=255;g=255;b=000;}	//gelb
			if (dropnodes_a[i].state == "halbfrei")  	{r=255;g=100;b=100;}	//rot
			if (dropnodes_a[i].state == "frei")  			{r=000;g=128;b=255;} 	//blau
		}	
																																			//eigentliches zeichnen: kreis mit farben rgb, position x,y und radius 3
		drawn_dropnodes_a[i].graphics.clear();
		drawn_dropnodes_a[i].graphics.beginFill(createjs.Graphics.getRGB(r,g,b,0.33)).drawCircle(dropnodes_a[i].x, dropnodes_a[i].y, 3);
		stage.addChild(drawn_dropnodes_a[i]);															//shape der stage hinzufuegen
		drawn_dropnodes_a[i].visible = true;															//dropnodes anzeigen oder nicht?
	} 																																	
}

function nearest_node_value(x,y) 																			//gibt von (x,y) aus naechsten dropnode zurück als index
{	var min_distance = Math.sqrt(2)*screen.width;
	var min_index = -1;
	for(i=0;i<dropnodes_a.length;i++) 																	//alle dropnodes durchgehen
	{	if(dropnodes_a[i].state != "nichtfrei") 													//nur dropnodes, die 'halbfrei' oder 'frei' beachten
	 	{	var tmp = calc_distance(x,y,dropnodes_a[i].x,dropnodes_a[i].y); //abstand zum dropnode berechnen
			if (tmp<min_distance) 																						//kleiner als bisheriges minimum?
			{ min_index = i;																								//ja, also index des dropnodes speichern
				min_distance = tmp; 																						//kleinsten abstand abspeichern
			}
		}
	}
	if (min_index==-1)  
			knapsack_full(); //alert("\n\n\nDer Rucksack ist voll!\n\n\n");			//kein einziger dropnode ist frei/halbfrei	
	else
		 	return min_index; 																							//naehester dropnode als index vom dropnode array zurueckgeben
		
}

function nearest_node_to_place_value(item_shape,item_png,target_index,item_volume)//dropnode finden, der mittig unter das item sein wird
{	var needed_dn_number = 2*item_volume+1; 														//item slot enthaelt 3 dropnodes, also anzahl benoetigter slots sind  volumen des items mal 2 + 1
	var vol = item_shape.volumen;
	var freespace_front=0; 																							//freie/halbreie dropnodes vorne 	vom index 'target_index' im array der dropnodes durchgehen # vorne-> + uhrzeigersinn
	var freespace_back=0;																								//                         hinten           																								 # hinten-> - uhreigersinn
	var index_front = 1; 																								//bei 1 anfangen, weil 'target_index' schon bei der auswahl nur frei/halbfrei sein kann
	while( index_front < needed_dn_number & dropnodes_a[(target_index+index_front)%dropnode_quantity].state!="nichtfrei") //nur freie/halbfreie nach vorne durchgehen
	{	freespace_front++; 
		index_front++; 
	}
	var index_back = 1; 																								//bei 1 anfangen, weil 'target_index' schon bei der auswahl nur frei/halbfrei sein kann
																																			//nur freie/halbfreiedropnodes nach hinten durchgehen
	while( index_back < needed_dn_number && dropnodes_a[(target_index-index_back+dropnode_quantity)%dropnode_quantity].state!="nichtfrei") 
	{	freespace_back++; 
		index_back++;
	}
	var freespace_sum = freespace_back + freespace_front +1;
																																			//nicht genug platz
	if (freespace_sum < needed_dn_number) 
	{	reposition_item(item_shape,item_png,-1); 														//nicht genug dropnodes
	}
																																			//genug platz aber nur hinten
	if (freespace_back  >= needed_dn_number && freespace_front ==0)
	{ reposition_item(item_shape,item_png,dropnodes_a[(target_index-parseInt(freespace_back*0.5+0.5)+dropnode_quantity)%dropnode_quantity]); 
		fill_dropnodes((target_index-parseInt(freespace_back*0.5+0.5)+dropnode_quantity)%dropnode_quantity,vol,item_shape.ID);
	}
																																			//genug platz aber nur vorne
	if (freespace_front >= needed_dn_number && freespace_back ==0)
	{ reposition_item(item_shape,item_png,dropnodes_a[(target_index+parseInt(freespace_front*0.5+0.5))%dropnode_quantity]); 
	 	fill_dropnodes((target_index+parseInt(freespace_front*0.5+0.5))%dropnode_quantity,vol,item_shape.ID);
	}
																																			//passt genau bei 'target_index'
	if (freespace_front >= vol && freespace_back >= vol) 
	{	reposition_item(item_shape,item_png,dropnodes_a[target_index]);
		fill_dropnodes(target_index,vol,item_shape.ID);
	}
																																			//passt, aber nur wenn nach hinten verschoben
	if (freespace_front < vol && freespace_sum >= needed_dn_number) 
	{	reposition_item(item_shape,item_png,dropnodes_a[(target_index-(vol-freespace_front)+dropnode_quantity)%dropnode_quantity]);
		fill_dropnodes((target_index-(vol-freespace_front)+dropnode_quantity)%dropnode_quantity,vol,item_shape.ID);
	}
																																			//passt, aber nur wenn nach vorne verschoben
	if (freespace_back < vol && freespace_sum >= needed_dn_number) 
	{	reposition_item(item_shape,item_png,dropnodes_a[(target_index+(vol-freespace_back))%dropnode_quantity]);
		fill_dropnodes((target_index+(vol-freespace_back))%dropnode_quantity,vol,item_shape.ID);
	}
}

function empty_dropnodes(ID) 																					//alle dropnodes besitzer loeschen und via funktionsweitergabe die status dementsprechend aktuallisieren
{ for(i=0;i<dropnodes_a.length;i++) 																	//alle dropnodes durchgehen
	{	if(dropnodes_a[i].ownerID==ID)	dropnodes_a[i].ownerID  = -1; 		// alle dropnodes besitzer   
		if(dropnodes_a[i].ownerID_==ID) dropnodes_a[i].ownerID_	= -1;			//			löschen  					  	
	}
	update_dropnodes(); 																								//dort wird abhaengig von den besitzern der jeweilige dropnode-status aktuallisiert (frei/halbfrei/nichtfrei)
}	
		
function update_dropnodes() 																					//die status aller dropnodes aktuallisieren
{	for(i=0;i<dropnodes_a.length;i++) 																	//alle dropnodes durchgehen
	{ if(dropnodes_a[i].ownerID!=-1&&dropnodes_a[i].ownerID_!=-1) 
		{	dropnodes_a[i].state="nichtfrei";																//zwei besitzer -> nichtfrei
			
		}
		if(dropnodes_a[i].ownerID!=-1&&dropnodes_a[i].ownerID_==-1) dropnodes_a[i].state="halbfrei";//ein besitzer -> halbfrei
		if(dropnodes_a[i].ownerID==-1&&dropnodes_a[i].ownerID_!=-1) dropnodes_a[i].state="halbfrei";//ein besitzer -> halbfrei
		if(dropnodes_a[i].ownerID==-1&&dropnodes_a[i].ownerID_==-1) dropnodes_a[i].state="frei";		//kein besitzer -> frei
	}
}
		
function fill_dropnodes(ziel_index, vol,ID) 													//eigentlichees einfuegen des items mit volumen 'vol'
{	for(i=0; i<vol;i++) 																								//dropnodes ausser die kanten, das sind dropnodes, die genau am rand der dann abgesetzten items sind
	{	var tmp = (ziel_index+i)%dropnode_quantity; 											//nach vorne auffuellen mit hilfe von index 'i'
		dropnodes_a[tmp].state = "nichtfrei";
		dropnodes_a[tmp].ownerID = ID;  																	// besitzer setzen auf beiden variablen,      
		dropnodes_a[tmp].ownerID_ = ID;																		//	   weil dann 'nichtfrei' statt 'halbfrei' 
		tmp = (ziel_index-i+dropnode_quantity)%dropnode_quantity; 				//nach hinten auffuellen mit hilfe von index 'i'
		dropnodes_a[tmp].ownerID = ID;  
		dropnodes_a[tmp].ownerID_ = ID;
		dropnodes_a[tmp].state = "nichtfrei"; 
	}
	update_dropnodes(); 																								//die status aller dropnodes aktuallisieren
																																			//kanten extra behandeln
	var edge_front = (ziel_index+vol)%dropnode_quantity; 								//vordere kante als index des dropnodes-arrays
	if(dropnodes_a[edge_front].state == "frei") 												//vordere kante ist freier dropnode
	{	dropnodes_a[edge_front].ownerID = ID;  														// besitzer setzen
		dropnodes_a[edge_front].ownerID_ = -1; 														// zweite besitzervariable bleibt leer = -1 = kein besitzer
	}
	else if (dropnodes_a[edge_front].state == "halbfrei")								//vordere kante ist halbfreier dropnode
	{ //if (dropnodes_a[edge_front].ownerID!=-1 && dropnodes_a[edge_front].ownerID_!=-1) alert("ownerid not free1"); //fehlermeldung
																																			//da diese kante 'halbfrei' ist, muessen sich zwei besitzer den dropnode teilen,
																																			//die freie besitzervariable (wenn -1 ) wird mit der ID des neuen besitzers gefuellt:
		if (dropnodes_a[edge_front].ownerID==-1) dropnodes_a[edge_front].ownerID=ID;  
		if (dropnodes_a[edge_front].ownerID_==-1) dropnodes_a[edge_front].ownerID_=ID;
	}
	var edge_back = (ziel_index-vol+dropnode_quantity)%dropnode_quantity;//hintere kante als index des dropnodes-arrays
	if(dropnodes_a[edge_back].state == "frei") 													//hintere kante ist freier dropnode
	{	dropnodes_a[edge_back].ownerID = ID; 
		dropnodes_a[edge_back].ownerID_ = -1;
	}
	else if (dropnodes_a[edge_back].state == "halbfrei") 								//hintere kante ist kante ist halbfreier dropnode
	{	//if (dropnodes_a[edge_back].ownerID!=-1 && dropnodes_a[edge_back].ownerID_!=-1) alert("ownerid not free2"); //fehlermeldung
	  																																	//da diese kante 'halbfrei' ist, muessen sich zwei besitzer den dropnode teilen,
																																			//die freie besitzervariable (wenn -1 ) wird mit der ID des neuen besitzers gefuellt:
		if (dropnodes_a[edge_back].ownerID==-1) dropnodes_a[edge_back].ownerID=ID;
		if (dropnodes_a[edge_back].ownerID_==-1) dropnodes_a[edge_back].ownerID_=ID;
	}
	update_dropnodes(); 																								//die status aller dropnodes aktuallisieren
}


function knapsack_full()
{		NOOP();																														//codegeruest fuer erweiterungen, wird schon woanders aufgerufen, nicht einfach so loeschen!
}