var stage;
all_picture_names_1='baguette-655335_960_720.png;banana-25339__180.png;bread-32168__180.png;cake-25388__180.png;candy-304393__180.png;carrot-320081_960_720.png;chard-37215__180.png;chocolate-163477__180.png;dessert-161066__180.png;drink-1012366_960_720.png;egg-24404__180.png;fennel-732375__180.png;fish-30828__180.png;green-214133__180.png;milk-999919__180.png;muesli-617710__180.png;noodles-649510__180.png;turkey-296601_180.png';
all_picture_names_2='chess.png;chill.png;cinema.png;circus.png;comp.png;foot.png;go-kart.png;golf.png;lesen.png;madn.png;mhoer.png;mspiel.png;photo.png;schwimmen.png;skate.png;sport.png;tennis.png;theater.png';
all_picture_names_3='bklammer.png;blstift.png;bustifte.png;essen.png;fueller.png;gdreieck.png;heft.png;kakao.png;kreide.png;lineal.png;locher.png;pins.png;pinsel.png;radier.png;schere.png;schloss.png;trechner.png;zirkel.png';

//Register Init function with the MTLG framework
MTLG.addGameInit((pOptions) => {
  //set globals
  stage = MTLG.gibStage();
  stage.mouseMoveOutside = false;
  stage.addEventListener("touchstart",fullscreen);

  MTLG.lc.registerMenu(choose_players);
  MTLG.lc.registerLevel(spiel_start, ()=>1);
});
