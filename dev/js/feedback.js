var back_ground_check = new createjs.Shape(); 																														//back_ground für chekckgame_modus
back_ground_check.graphics.beginFill("#123456").drawRect(0, 0, screen.width, screen.height);
back_ground_check.alpha=0.7;
var in_feedbackmenu_boolean = false;
var back_container = new createjs.Container();																														//container für  Zurück_Button
var to_next_level = 4;																																										// Variable für n�chstes Spiel überspringen
var number_of_bad_try =0;
var cheat_mode_on = false;																																								//es ist nur eine Hilf-Variable, wenn True, dann immer Weiter klicken kann

function check_game(stage)
{	in_feedbackmenu_boolean = true;


	var stage_width = screen.width;
	var stage_height = screen.height;

	stage.addChild(back_ground_check);
	createjs.Touch.enable(stage);


	var tipp_4 = new createjs.Bitmap('img/bilder/hinweise4.png');																								//spielen mit n�chstem Level
	tipp_4.x=screen.width/2-300;
	tipp_4.y=screen.height/2-200;
	tipp_4.visible=false;
	stage.addChild(tipp_4);
	var tipp_5 = new createjs.Bitmap('img/bilder/hinweise5.png');																								// noch nicht genug Sterne sammeln
	tipp_5.x=tipp_4.x;
	tipp_5.y=tipp_4.y;
	tipp_5.visible=false;
	stage.addChild(tipp_5);
	var tipp_6 = new createjs.Bitmap('img/bilder/end_bild.png');																								// Szenario komplett durchgespielt
	tipp_6.x=screen.width/2-550;
	tipp_6.y=tipp_4.y-100;
	tipp_6.visible=false;
	stage.addChild(tipp_6);

	if(scenario_index < scenario_all_merged_array[scenario_type-1].length-1)																// Noch nicht letztes Level erreichen
	{
		if(star_quantity>=scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity)				//max.Anzahl der Sterne im aktuellen Level erreichchen
		{
				tipp_4.visible=true;
		}

	 	if(star_quantity<scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity )				//max.Anzahl der Sterne im aktullen Level nicht erreichen
		{
				tipp_5.visible=true;
	 	}
  }
  else																																																		//im Letzten Level
  {
  	if(star_quantity>=scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity)
		{
				tipp_6.visible=true;
		}

	 	if(star_quantity<scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity )
		{
				tipp_5.visible=true;
	 	}
  }

	var restart = new createjs.Shape();
	var text_restart = new createjs.Text("Neues Spiel", "bold 36px Gabriola", "#FFFFFF");
	text_restart.textBaseline = "middle";
	text_restart.x = 20;
	text_restart.y = 100/2;
	var restart_container = new createjs.Container();																													//container für Neues-Spiel-button
	restart_container.x = screen.width*3/4 ;
	restart_container.y = screen.height*3/4-400;
	restart_container.addChild(restart, text_restart);
	if(scenario_index < scenario_all_merged_array[scenario_type-1].length-1)																	// Noch nicht letztes Level erreichen
	{
			if(star_quantity < scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity)				//Man kann immer neues Szenario w�hlen,wann man die L�sung von akt.Level noch nicht hat
			{
				restart.graphics.beginFill("red").drawRoundRect(0,0, 300, 100,20);
				restart_container.alpha=0.6;
				restart_container.addEventListener("click", click_restart);

			}
			if(star_quantity >= scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity)			//max.Anzahl der Sterne erreicht dann kann nicht Neues-Spiel-Button klicken
			{
				restart.graphics.beginFill("grey").drawRoundRect(0,0, 300, 100,20);
				restart_container.alpha=0.3;
			}
	}
	else																																																			//im letzten Level
	{
			if(star_quantity < scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity)				//Man kann immer neues Szenario w�hlen,wann man die L�sung von akt.Level noch nicht hat
			{
				restart.graphics.beginFill("red").drawRoundRect(0,0, 300, 100,20);
				restart_container.alpha=0.6;
				restart_container.addEventListener("click", click_restart);

			}
			if(star_quantity >= scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity)			//max.Anzahl der Sterne erreicht dann kann nicht Neues-Spiel-Button klicken
			{
			restart.graphics.beginFill("red").drawRoundRect(0,0, 300, 100,20);
			restart_container.alpha=0.6;
			restart_container.addEventListener("click", click_reboot);																						//Neus-Spiel-Button funtioniert wie refesh
			}
	}
	if (in_feedbackmenu_boolean)	stage.addChild(restart_container);


	var back = new createjs.Shape();																																					//Zurück-Button
	var text_back = new createjs.Text("Zurück", "bold 36px Gabriola", "#FFFFFF");
	text_back.textBaseline = "middle";
	text_back.x = 20;
	text_back.y = 100/2;

	back_container.x = restart_container.x ;
	back_container.y = restart_container.y+200+60;
	back_container.addChild(back, text_back);

	back.graphics.beginFill("blue").drawRoundRect(0,0, 300, 100,20);
	back_container.alpha=0.5;
	back_container.addEventListener("click", click_back);
	function click_back(event) 																																								//ClickEvent für Zurück Button
	{

		in_feedbackmenu_boolean = false;																																					//nicht mehr im Feedback_modus
		stage.removeChild(tipp_4);																																								//feedback gelöscht
		stage.removeChild(tipp_5)
		stage.removeChild(tipp_6)
		stage.removeChild(back_ground_check);																																			//feedback_background gelöscht
		stage.removeChild(back_container);																																				//zurück-button gelöscht
		stage.removeChild(restart_container);																																			//Neues-Spiel-button gelöscht
		stage.removeChild(next_container);																																				//Weiter-button gelöscht


		for(i=0;i<item_shapes_a.length;i++)																																				//alle Shapes wieder auf Spielfeld
					item_shapes_a[i].visible=true;
		for(i=0;i<item_picture_a	.length;i++)																																		//alle Item-Bilder wieder auf Spielfeld
					item_picture_a[i].visible=true;
		check_container.visible=true;
		angel.visible=need_angel_boolean;
		update_dropnodes();


	}

	if(in_feedbackmenu_boolean)	stage.addChild(back_container);



	var next = new createjs.Shape();
	var next_text = new createjs.Text("Weiter", "bold 36px Gabriola", "#FFFFFF");
	next_text.textBaseline = "middle";
	next_text.x = 20;
	next_text.y = 100/2;
	var next_container = new createjs.Container();																															//Next Container
	next_container.x = restart_container.x ;
	next_container.y = restart_container.y+100+30;
	next_container.addChild(next, next_text);																																		//Next Button
	if(scenario_index < scenario_all_merged_array[scenario_type-1].length-1)
	{
			if(star_quantity < scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity && !cheat_mode_on)
			{
				next.graphics.beginFill("grey").drawRoundRect(0,0, 300, 100,20);
				next_container.alpha=0.3;
			}
																																																							//nur Weiter klicken kann, wenn man richtige L�sung für das akt.Level findet oder cheat_mode_on==true
			if(star_quantity >= scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity || cheat_mode_on)
			{
				next.graphics.beginFill("green").drawRoundRect(0,0, 300, 100,20);
				next_container.alpha=0.6;
				next_container.addEventListener("click", click_next);
			}
	}
	else
	{
			next.graphics.beginFill("grey").drawRoundRect(0,0, 300, 100,20);
			next_container.alpha=0.3;

	}



	if(in_feedbackmenu_boolean)	stage.addChild(next_container);

	if(star_quantity<scenario_all_merged_array[scenario_type-1][scenario_index].optimum_quantity)								//keine richtige L�sung bei dem Prüfen
			number_of_bad_try++;

}

//Event-Funktionen
function click_restart(event) 																																								//ClickEvent für Neues Spiel Button
{			in_feedbackmenu_boolean = false;
			for(i=0;i<items_within_knapsack.length;i++)																															//Rucksack leeren
			{	remove_from_knapsack(items_within_knapsack[i]);
			}

			stage.removeAllChildren();
			stage.removeAllEventListeners();
			refresh_all();
			scenario_index=3;																																												//scenario_index wieder gesetzt, hier spielt man direkt mit 4 Items
			turn_speed = 3; 																																												//Geschwindigkeit der Sterne wieder auf 3 setzen
			szenario(stage)

}

function click_next(event)																																										//Click Event für Next_button
{ 		in_feedbackmenu_boolean = false;
			stage.removeAllChildren();
			stage.removeAllEventListeners();
			refresh_all();
			if(scenario_index+to_next_level<scenario_all_merged_array[scenario_type-1].length)											//Level überspringen wenn der Sprung noch geht
				scenario_index=scenario_index+to_next_level;
			else
				scenario_index=scenario_all_merged_array[scenario_type-1].length-1;																		// wenn der Sprung nicht mehr geht, dann gehe in direkt n�chstes Level bis zum Ende (Level 17)
			turn_speed = 3;
			spiel_start()

}

function click_reboot(event)
{
				location.reload();																																										//wie F5
}
